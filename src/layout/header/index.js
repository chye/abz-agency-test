import React, {Component} from 'react';
import {Container} from 'reactstrap';
import { ReactComponent as Logo }  from '../../media/logo/logo.svg';
import Links from '../links';
import Login from '../login';
import  {ReactComponent as LineMenu} from '../../media/icons/line-menu.svg';
import PropTypes from 'prop-types';

class Header extends Component {
    render() {
        return (
            <header>
                <Container>
                    <div className={'header-block'}>
                        <nav>
                            <div className="logo">
                                <a href="/#"> <Logo/> </a>
                            </div>
                            <Links/>
                            <Login/>
                           <div onClick={()=>this.props.handleShowSideBar()} className={'burger'}><LineMenu/></div>
                        </nav>
                    </div>
                </Container>
            </header>

        );
    }
}

Header.propTypes = {
    handleShowSideBar: PropTypes.func
};

export default Header;