import {LOGIN_LAYOUT_ACTIONS} from '../../../redux/actions';

const DEFAULT_STATE = {
    user: {data:{}, status:{ready: null, req: null, error: null}},

};

const loginLayoutReducer = (state = DEFAULT_STATE, action) =>
{
    switch (action.type)
    {
        case LOGIN_LAYOUT_ACTIONS.LOGIN_LAYOUT_GET_LOGIN_USER_REQUEST:
            return Object.assign({}, state, {
                user: {data:{}, status:{ready: null, req: true, error: null}},
            });
        case LOGIN_LAYOUT_ACTIONS.LOGIN_LAYOUT_GET_LOGIN_USER_OK:
            return Object.assign({}, state, {
                user: {data:action.user, status:{ready: true, req: null, error: false}}
            });
        case LOGIN_LAYOUT_ACTIONS.LOGIN_LAYOUT_GET_LOGIN_USER_FAIL:
            return Object.assign({}, state, {
                user: {data:{}, status:{ready: false, req: false, error: action.error}},
            });

        default:
            return state;
    }
};
export default loginLayoutReducer;
