import {LOGIN_LAYOUT_ACTIONS} from '../../../redux/actions';
import SERVER from "../../../config/server";

export const getUserInfo = (dispatch, user_id) => {
    dispatch({type: LOGIN_LAYOUT_ACTIONS.LOGIN_LAYOUT_GET_LOGIN_USER_REQUEST});
    fetch(`${SERVER.address}/users/${user_id}`)
        .then( response => response.json())
        .then(data => {
            if(data.success) {
                dispatch({type: LOGIN_LAYOUT_ACTIONS.LOGIN_LAYOUT_GET_LOGIN_USER_OK, user: data.user });
            } else {
                dispatch({type: LOGIN_LAYOUT_ACTIONS.LOGIN_LAYOUT_GET_LOGIN_USER_FAIL, error: data.message});
            }
        })
        .catch(error => dispatch({type: LOGIN_LAYOUT_ACTIONS.LOGIN_LAYOUT_GET_LOGIN_USER_FAIL, error}))
};