import * as methods from './methods';
export const mapStateToProps = (state) => {
    return {
        user: state.login.user,

    }
};

export const mapDispatchToProps = (dispatch) => {
    return {
        handleGetUserInfo: (user_id) => methods.getUserInfo(dispatch, user_id),

    }
};
