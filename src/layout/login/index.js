import React, {Component, Fragment}  from 'react';
import {connect} from "react-redux";
import {mapStateToProps, mapDispatchToProps} from './store/map_to_props';
import  {ReactComponent as SignOut} from '../../media/icons/sign-out.svg';
import PropTypes from 'prop-types';

class Login extends Component{

    componentDidMount() {
        this.props.handleGetUserInfo(1)
    }

    render() {
        const {user, sidebar} = this.props;
        return (

                    <div className={'login'}>
                        {user.status.ready ?
                            <Fragment>
                                <div className="info-block">
                                    <div className="name">{user.data.name}</div>
                                    <div className="email">{user.data.email}</div>
                                </div>
                                <div className="img-block"><img src={user.data.photo} alt="logged_user_img"/></div>
                                {!sidebar && <button className="sing_out"><SignOut/></button>}
                            </Fragment>
                            : user.status.error ? user.status.error : "TRY TO LOGGING"}
                    </div>

        );
    }
}

Login.propTypes = {
    sidebar: PropTypes.bool,
    user: PropTypes.object
};
export default connect(mapStateToProps, mapDispatchToProps)(Login);