import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import Links from "../links";
import  {ReactComponent as Logo} from '../../media/logo/logo.svg';
import  {ReactComponent as Mail} from '../../media/icons/mail.svg';
import  {ReactComponent as Phone} from '../../media/icons/phone.svg';
import  {ReactComponent as CellPhone} from '../../media/icons/cellphone.svg';
import  {ReactComponent as FaceBook} from '../../media/icons/facebook.svg';
import  {ReactComponent as Linkedin} from '../../media/icons/linkedin.svg';
import  {ReactComponent as Instagram} from '../../media/icons/instagram.svg';
import  {ReactComponent as Twitter} from '../../media/icons/twitter.svg';
import  {ReactComponent as Pinterest} from '../../media/icons/pinterest.svg';

const contacts = [
    {
        icon: <Mail/>,
        txt: 'work.of.future@gmail.com'
    },
    {
        icon: <Phone/>,
        txt: '+38 (050) 789 24 98 '
    },
    {
        icon: <CellPhone/>,
        txt: '+38 (095) 556 08 45'
    }
];

const siteMap = [
    ['News','Blog', 'Partners', 'Shop'],
    ['Overview', 'Design', 'Code', 'Collaborate'],
    ['Tutorials', 'Resources', 'Guides', 'Examples'],
    ['FAQ', 'Terms', 'Conditions', 'Help']
];

const socialsIcons = [
    {
        icon: <FaceBook/>,
        href: "./#"
    },
    {
        icon: <Linkedin/>,
        href: "./#"
    },
    {
        icon: <Instagram/>,
        href: "./#"
    },
    {
        icon: <Twitter/>,
        href: "./#"
    },
    {
        icon: <Pinterest/>,
        href: "./#"
    }];

const Footer = () => {
    return (
         <footer>
            <Container>
                <nav>
                    <div className="logo"><Logo/></div>
                    <Links/>
                </nav>
                <div className="middle">
                    <Row>
                        <Col md={4}>
                            <div className="block-info">
                                <ul>
                                    {contacts.map((contact, i) =>
                                        <li key={i}>
                                            <div className="icon">{contact.icon}</div>
                                            <div className="txt">{contact.txt}</div>
                                        </li>
                                    )}
                                </ul>
                                <span className={'copy'}>© abz.agency specially for the test task</span>
                            </div>
                        </Col>
                        {siteMap.map((col, i) => {
                            return (<Col md={2} key={i}>
                                    <ul className={'site-map'}>
                                        {col.map((link, j) =>
                                            <li key={j}><a href="./#">{link}</a></li>
                                        )}
                                    </ul>
                                    </Col>)
                        })}
                        <ul className="social-icons">
                            {socialsIcons.map((item,i) =>
                                <li key={i}><a href={item.href}>{item.icon}</a></li>
                            )}
                        </ul>

                    </Row>
                </div>
            </Container>
         </footer>
    )
};
export default Footer;