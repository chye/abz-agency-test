import React, {Component} from 'react';
import Links from '../links';
import Login from '../login';

class SideBar extends Component {
    render() {
        const {showSidebar} = this.props;
        return (
            <aside className={showSidebar? 'showBar' : 'closeBar'} >
                    <div className={'sidebar-block'}>
                        <Login sidebar={true}/>
                        <Links sidebar={true}/>
                    </div>
                    <div className="bg" onClick={()=>this.props.handleShowSideBar()}/>

            </aside>

        );
    }
}

export default SideBar;