import React  from 'react';
import PropTypes from 'prop-types';
import AnchorLink from 'react-anchor-link-smooth-scroll';

const Links = (props) => {
    const {sidebar} = props;
        return (
            <ul className="links">
                <li>
                    <AnchorLink href="#about-me">About me</AnchorLink>
                </li>
                <li><AnchorLink href="#my-relationships">Relationships</AnchorLink></li>
                <li><AnchorLink href="#requirements">Requirements</AnchorLink></li>
                <li><AnchorLink href="#users">Users</AnchorLink></li>
                <li><AnchorLink href="#registration">Sign Up</AnchorLink></li>
                {sidebar && <li><a href="/#">Sign Out</a></li>}
            </ul>
        );
};

Links.propTypes = {
    sidebar : PropTypes.bool
};

export default Links;