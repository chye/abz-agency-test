import React, {Component} from 'react';
import './styles/style.scss';
import Header from './layout/header';
import SideBar from "./layout/sidebar";
import Footer from "./layout/footer/index"
import MainPage from "./pages/main";

class App extends Component {
    state={
        showSideBar: false
    };

  handleShowSidebar = () =>{
    this.setState({showSideBar: !this.state.showSideBar});
  };
    render (){
      const {showSideBar} = this.state;

      return(
          <div className="App">
              <Header handleShowSideBar={this.handleShowSidebar}/>
              <SideBar handleShowSideBar={this.handleShowSidebar} showSidebar={showSideBar}/>
              <MainPage/>
              <Footer/>
          </div>
      )
  }
}

export default App;
