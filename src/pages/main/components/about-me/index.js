import React from 'react';
import {Container} from 'reactstrap';
import ManMobile from '../../../../media/images/man-mobile.svg'
import checkElementTextOutSide from "../../../../components/check_text_outside";

const AboutMe = () => {
    return(
        <section className={'about-me'} id={'about-me'} >
            <Container>
                <h2>Let's get ac quainted</h2>
                <div className="content">
                    <div className="icon-block">
                        <img src={ManMobile} alt="man-mobile"/>
                    </div>
                    <div className="info-block">
                        <h3>I am cool frontend developer</h3>
                        {checkElementTextOutSide('When real users have a slow experience on mobile, they\'re much less likely to find what they are looking for or purchase from you in the future. For many sites this equates to a huge missed opportunity, especially when more than half of visits are abandoned if a mobile page takes over 3 seconds to load.', 304)}
                        {checkElementTextOutSide('Last week, Google Search and Ads teams announced two new speed initiatives to help improve user-experience on the web.', 118 )}
                        <a href="/#" >Sign Up</a>
                    </div>
                </div>
            </Container>

        </section>
    )
};


export default AboutMe;