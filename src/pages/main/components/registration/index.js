import React, {Component} from 'react';
import {Container, Modal, ModalBody, ModalHeader, ModalFooter} from 'reactstrap'
import Icon from '../../../../media/icons/caret-down.svg'
import UploadIcon from '../../../../media/icons/upload.svg'
import {getFileSize} from "../../../../components/photoValidates";
import PropTypes from 'prop-types';



const renderPositions = (positions, close, getPosition) => {
    if(positions.status.ready && Array.isArray(positions.data) && positions.data.length > 0){
     return (<div className={'select-container'}>
                 <div className={'position disable'} onClick={close}> Selected Item </div>
                {positions.data.map(position =>
                    <div className={'position'}  onClick={()=>getPosition(position.name, position.id)} key={position.id}> {position.name} </div>
                )}
            </div>)
    }else{
     return   (<div className={'select-container'}> <div className={'position disable'} onClick={close}> Don't have positions</div> </div>)
    }
};


class Registration extends Component {
    state={
        nameFile: 'Upload your photo',
        photo: {value:'', error: ''},
        name: {value:'', error:''},
        email: {value:'', error: ''},
        phone: {value:'', error: ''},
        position_id:{value:'', error: ''},
        show_positions: false,
        position_value: '',
        all_fields: [],
    };

   componentDidMount() {
       this.props.handleGetPositions();
   }

   componentDidUpdate(prevProps, prevState, snapshot) {
       //ADD ERRORS FROM BACKEND
       if(prevProps.new_user.ready === null && this.props.new_user.ready === false && Object.keys(this.props.new_user.error).length > 0){
           const ERRORS = this.props.new_user.error;
           //if status STATUS 422
           if(typeof ERRORS !== 'string'){
               Object.keys(ERRORS).forEach(error => {
                   if(error === 'photo'){
                       this.setState( { [error]: {error: ERRORS[error], value: this.state[error].value}})
                   }else{
                       this.setState( { [error]: {error: ERRORS[error][0], value: this.state[error].value}})
                   }
               })
           }else{
               this.setState({phone: {value: this.state.phone.value, error: ERRORS}, email: {value: this.state.email.value, error: ERRORS}})
           }
       }
       // CLEAR ALL INPUT STATES IF ADD NEW USER
       if(!prevProps.new_user.ready && this.props.new_user.ready){
           this.setState({
               photo: {value:'', error: ''},
               name: {value:'', error:''},
               email: {value:'', error: ''},
               phone: {value:'', error: ''},
               position_id:{value:'', error: ''},
               position_value: '',
               nameFile: 'Upload your photo',
           })
       }
   }

   handleCheckAllField = () => {
       const {name, photo, email, phone, position_id} = this.state;
       return name.value !== '' && position_id.value !== '' && photo.value !== '' && email.value !== '' && phone.value !== '';
   };

   handleUploadImg = (e) => {
        e.preventDefault();
        if(e.target.files[0]){
            let {all_fields} = this.state;
            let nameFile = e.target.files[0].name;
            let type = e.target.files[0].type;
            let size = getFileSize(e.target.files[0].size);
            let photo = e.target.files[0];
            let errorsPhoto = [];

            type !=='image/jpeg' && errorsPhoto.push(' The photo may  be only  in jpg format ');
            !size.upTo &&  errorsPhoto.push(size.message);


            if(errorsPhoto.length === 0){
                all_fields.indexOf('photo') === -1 && all_fields.push('photo');
                this.setState({nameFile, photo: {value: photo, error: ''},  all_fields});
            }else{
                const new_all_fields = all_fields.filter(val => val !== 'photo');
                this.setState({photo:{error: errorsPhoto, value: '', nameFile: 'Upload your photo', all_fields : new_all_fields}});
            }
        }
    };

   handleChangeValueInput = (e) => {
       e.preventDefault();
       let {all_fields} = this.state;
       //add field`s name to array all_field in order to know which field isn't empty
       if( e.target.value !== ''){
           all_fields.indexOf(e.target.name) === -1 && all_fields.push(e.target.name);
       }else{
           all_fields = all_fields.filter(val => val !== e.target.name)
       }
       this.setState({all_fields, [e.target.name] : {value: e.target.value, error: ''}});
   };

   handleGetPositions = () => {
        this.setState({show_positions: !this.state.show_positions});
    };

   handleGetPosition = (position_value, position_id) => {
        let {all_fields} = this.state;

        all_fields.indexOf('position_id') === -1 && all_fields.push('position_id');

        this.setState({position_value, position_id: {value: position_id, error: ''}, show_positions: !this.state.show_positions, all_fields});
    };

   handleSubmit = () => {
       const {name, photo, email, phone, position_id} = this.state;
        if(this.handleCheckAllField()){
            this.props.handlePostNewUser({name: name.value, photo: photo.value, email: email.value, phone: phone.value, position_id: position_id.value});
        }else{
            //Here i am checking what field is empty
            name.value === "" && this.setState({name: {error:'The name must be at least 2 characters.', value:' '}});
            email.value === "" && this.setState({email: {error:'The email must be a valid email address.', value:' '}});
            phone.value === "" && this.setState({phone: {error: 'The phone field is required.', value:' '}});
            position_id.value === "" && this.setState({position_id: {error:'Select you position', value:' '}});
            photo.value === "" && this.setState({photo: {error: ['The photo may not be greater than 5 Mbytes'], value:' '}});
       }

   };

    render() {
        const {positions, handleToggleModal} = this.props;
        const {photo,name, phone, email, show_positions, position_value, position_id, all_fields} = this.state;

        return(
            <section className={'registration'} id={'registration'}>
                <Container>
                    <div className="content">
                        <h2>Register to get a work</h2>
                        <h5>Attention! After successful registration and alert, update the list of users in the block from the top</h5>
                        <form action="#">
                            <div className="small-blocks">
                                <div className={`input-block ${name.error !== '' ? 'error':''}`}>
                                    <span>Name</span>
                                    <label>
                                        <input id={name} type="text" value={name.value} name={'name'} placeholder={"Your name"} onChange={this.handleChangeValueInput}/>
                                    </label>
                                    {name.error !== '' && <div className="txt-error">{name.error}</div>}
                                </div>
                                <div className={`input-block ${email.error !== '' ? 'error':''}`}>
                                    <span>Email</span>
                                    <label>
                                        <input type="text" value={email.value} name={'email'} placeholder={"Your email"} onChange={this.handleChangeValueInput}/>
                                    </label>
                                    {email.error !== '' && <div className="txt-error">{email.error}</div>}
                                </div>
                                <div className={`input-block ${phone.error !== '' ? 'error':''}`}>
                                    <span>Phone</span>
                                    <label>
                                        <input type="text" value={phone.value} name={'phone'} placeholder={"+38 (___)  ___ __ __"} onChange={this.handleChangeValueInput}/>
                                    </label>
                                    {phone.error !== '' && <div className="txt-error">{phone.error}</div>}
                                </div>
                            </div>
                            <div className="big-blocks">
                                <div className={`input-block ${show_positions ? 'show': ''} ${position_id.error !== '' ? 'error':''} `}>
                                    <div className={'input'}>
                                        <div className={`txt`}>{position_value === ''? 'Select your position' : position_value}</div>
                                        <div className="icon-block" onClick={this.handleGetPositions}><img src={Icon} alt="icon"/></div>
                                        {show_positions && renderPositions(positions, this.handleGetPositions, this.handleGetPosition)}
                                        {position_id.error !== '' && <div className="txt-error">{position_id.error}</div>}
                                    </div>
                                </div>
                                <div className={`input-block  ${photo.error !== '' ? ' error' : ""}`}>
                                    <div className="txt">{this.state.nameFile}</div>
                                    <div className={'file'}>
                                        <div className={'upload'} onClick={this.handleClickInputFile}><img src={UploadIcon} className="icon-block" alt={'icon-upload'}/><div className="icon-text">Upload</div></div>
                                        <input  onChange={e => this.handleUploadImg(e)} type="file"/>
                                    </div>
                                    <div className={`info ${photo.error !== '' ? ' error' : ""}`}>
                                        <div className="txt">
                                        {!Array.isArray(photo.error) && photo.error.length === 0 ? "File format jpg  up to 5 MB, the minimum size of 70x70px" : photo.error.map((error, i)=><div key={i}>{error}</div>)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className={'d-flex justify-content-center'}>
                                <button className={`bt  ${all_fields.length === 5 ? 'primary' :'disabled'}`}  onClick={e => {e.preventDefault(); all_fields.length === 5 && this.handleSubmit()}}> Sign Up </button>
                            </div>
                        </form>
                    </div>
                </Container>

                <Modal isOpen={this.props.show_modal} className={'modal-registration'}>
                    <ModalHeader>Congratulations</ModalHeader>
                    <ModalBody>
                        You have successfully passed the registration
                    </ModalBody>
                    <ModalFooter><button onClick={handleToggleModal}>ok</button></ModalFooter>
                </Modal>
            </section>
        )
    }

};

Registration.propTypes = {
    handleGetPositions: PropTypes.func,
    handlePostNewUser: PropTypes.func,
    handleToggleModal: PropTypes.func,
    positions: PropTypes.object,
    new_user: PropTypes.object,
    show_modal: PropTypes.bool
};

export default Registration