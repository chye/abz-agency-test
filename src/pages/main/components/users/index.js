import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import PropTypes from 'prop-types';



const OurUsers = (props) =>{
    const {users, handleGetMoreUsers, next_link_users} = props;
    return(
        <section className={'our-users'} id={'users'}>
            <Container>
                <div className="content">
                    <h2>Our cheerful users</h2>
                    <h5>Attention! Sorting users by registration date</h5>
                    <Row>
                        {users.status.ready && Array.isArray(users.data) ? users.data.map(user =>
                            <Col md={4} sm={6} xs={12} key={user.id}>
                                <div className="user-block">
                                    <div className="icon-block"><img src={user.photo} alt={user.name}/></div>
                                    <div className="info-block">
                                        <h4 className="name">{user.name}</h4>
                                        <div className="position">
                                            {user.position}
                                        </div>
                                        <div className="email">
                                            {user.email}
                                        </div>
                                        <div className="phone-number">
                                            {user.phone}
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        ): <Col xs={12}><h4 className={'text-center'}>{users.status.error}</h4></Col>}
                    </Row>
                    {next_link_users && <button className={'bt secondary'} onClick={()=>handleGetMoreUsers()}>Show more</button>}
                </div>
            </Container>
        </section>
    )
};
OurUsers.propTypes = {
    users: PropTypes.object,
    next_link_users: PropTypes.bool,
    handleGetMoreUsers: PropTypes.func
};
export default OurUsers;