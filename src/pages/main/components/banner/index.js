import React from 'react';
import {Container} from 'reactstrap';
import checkElementTextOutSide from "../../../../components/check_text_outside";


const Banner = () => {
    return(
        <section className={'banner-top'} >
            <Container >
                <h1>Test assignment for Frontend Developer position</h1>
                {checkElementTextOutSide('We kindly remind you that your test assignment should be submitted as a link to github/bitbucket repository. Please be patient, we consider and respond to every application that meets minimum requirements. We look forward to your submission. Good luck!', 252)}
                <button className="bt primary">Sign Up</button>
            </Container>
        </section>
    )
};
export default Banner;