import React from 'react';
import {Container, Col, Row} from 'reactstrap';
import HTML_ICON from '../../../../media/images/html.svg';
import JS_ICON from '../../../../media/images/javascript.svg';
import CSS_ICON from '../../../../media/images/css.svg';
import checkElementTextOutSide from "../../../../components/check_text_outside";


const relationships = [
    {
        icon_src: HTML_ICON,
        h3_txt: 'I\'m in love with HTML',
        p_text: 'Hypertext Markup Language (HTML) is the standard markup language for creating web pages and web applications.'
    },
    {
        icon_src: CSS_ICON,
        h3_txt: 'CSS is my best friend',
        p_text: 'Cascading Style Sheets (CSS) is a  style sheet language used for describing the presentation of a document written in a markup language like HTML.'
    },
    {
        icon_src: JS_ICON,
        h3_txt: 'JavaScript is my passion',
        p_text: 'JavaScript is a high-level, interpreted programming language. It is a language which is also characterized as dynamic, weakly typed, prototype-based and multi-paradigm.'
    }
];

const MyRelationships = (props) => {

    return(
        <section className={'my-relationships'} id={'my-relationships'} >
            <Container>
                <div className="content">
                    <h2>About my relationships with web-development</h2>
                    <Row>
                        {relationships.map((relation, i)=>{
                            return(
                                <Col lg={4} key={i}>
                                    <div className="relation">
                                        <div className="icon-block">
                                            <img src={relation.icon_src} alt={relation.h3_txt}/>
                                        </div>
                                        <div className="text-block">
                                            <h3>{relation.h3_txt}</h3>
                                            {checkElementTextOutSide(relation.p_text, 168)}
                                        </div>
                                    </div>
                                </Col>
                            )
                        })}
                    </Row>
                </div>
            </Container>

        </section>
    )
};


export default MyRelationships;