import {MAIN_PAGE_ACTIONS} from '../../../redux/actions';
import SERVER from '../../../config/server'

export const getUsers = (dispatch, page = 1, userCount = 6) => {
    dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_USERS_REQUEST});
    fetch(`${SERVER.address}/users?page=${page}&count=${userCount}`)
        .then( response => response.json())
        .then(data => {
            if(data.success) {
                const result = data.users.sort((a, b) => {
                    if(a.id < b.id){
                        return 1
                    }
                    if(a.id > b.id){
                        return -1
                    }
                    return  0
                });
                const link_next = data.links.next_url && true;
                dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_USERS_OK, users: result, link_next});

            } else {
                dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_USERS_FAIL, error: data.message})
            }
        })
        .catch(error => dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_USERS_FAIL, error}))
};

export const getMoreUsers = (dispatch, page = 1 , arrayUsers = [], userCount = 6) => {
    dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_MORE_USERS_REQUEST});
    fetch(`${SERVER.address}/users?page=${page}&count=${userCount}`)
        .then( response => response.json())
        .then(data => {
            if(data.success) {
                const allUsers = arrayUsers;
                data.users.forEach(user=> allUsers.push(user));
                const result = allUsers.sort((a, b) => {
                    if(a.id < b.id){
                        return 1
                    }
                    if(a.id > b.id){
                        return -1
                    }
                    return  0
                });
                const link_next = data.links.next_url && true;
                dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_MORE_USERS_OK, users: result, link_next});

            } else {
                dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_MORE_USERS_FAIL, error: data.message})
            }
        })
        .catch(error => dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_USERS_FAIL, error}))
};

export const getPositions = (dispatch) => {
    dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_POSITIONS_REQUEST});
    fetch(`${SERVER.address}/positions`)
        .then( response => response.json())
        .then(data => {
            if(data.success) {
                dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_POSITIONS_OK, positions: data.positions});
            } else {
                dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_POSITIONS_FAIL, error: data.message})
            }
        })
        .catch(error => dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_POSITIONS_FAIL, error}))
};



export  const postNewUsers = (dispatch, newUserInfo) => {
    dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_POST_NEW_USERS_REQUEST});
    fetch(`${SERVER.address}/token`)
        .then( response => response.json())
        .then(data => {
            if(data.success) {

                const token = data.token;
                const {name, email, phone, photo, position_id } = newUserInfo;
                let formData = new FormData();


                formData.append('position_id', position_id);
                formData.append('name', name);
                formData.append('email', email);
                formData.append('phone', phone);
                formData.append('photo', photo);

                fetch(`${SERVER.address}/users`, {
                    method: 'POST',
                    body: formData,
                    headers: {
                        'Token': token, // get token with GET api/v1/token method
                    },
                })
                    .then(function(response) {
                        return response.json();
                    })
                    .then(function(data) {
                        if(data.success) {
                            dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_POST_NEW_USERS_OK});
                        } else {
                            if(data.fails){
                                dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_POST_NEW_USERS_FAIL, error: data.fails});
                            }else{
                                dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_POST_NEW_USERS_FAIL, error: data.message});
                            }

                        }
                    })
                    .catch(error => dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_POST_NEW_USERS_FAIL, error: error.message}))

            } else {
                dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_POST_NEW_USERS_FAIL, error: data.message});
            }
        })
        .catch(error => dispatch({type: MAIN_PAGE_ACTIONS.MAIN_PAGE_POST_NEW_USERS_FAIL, error: error.message}))
};