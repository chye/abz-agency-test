import * as methods from './methods';

export const mapStateToProps = (state) => {
    return {
        users: state.main.users,
        next_link_users: state.main.next_link_users,
        more: state.main.more,
        positions: state.main.positions,
        new_user: state.main.new_user

    }
};

export const mapDispatchToProps = (dispatch) => {
    return {
        handleGetUsers: (page, usersCount) => methods.getUsers(dispatch, page, usersCount),
        handleGetMoreUsers: (page, arrayUsers, userCount) => methods.getMoreUsers(dispatch, page, arrayUsers, userCount),
        handleGetPositions: ()=> methods.getPositions(dispatch),
        handlePostNewUser: (newUserInfo)=> methods.postNewUsers(dispatch, newUserInfo)
    }
};
