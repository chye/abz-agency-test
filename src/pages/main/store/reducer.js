import {MAIN_PAGE_ACTIONS} from '../../../redux/actions';

const DEFAULT_STATE = {
    users: {status:{req:null, ready: null, error: null}, data:{}},
    next_link_users: null,
    more: null,
    positions: {status:{req:null, ready: null, error: null}, data:[]},
    new_user: {req:null, ready: null, error: null}
};

const mainPageReducer = (state = DEFAULT_STATE, action) =>
{
    switch (action.type)
    {
        case MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_USERS_REQUEST:
            return Object.assign({}, state, {
                users: {status:{req:true, ready: null, error: null}, data:{}},
                next_link_users: null,
            });
        case MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_USERS_OK:
            return Object.assign({}, state, {
                users: {status:{req: false, ready: true, error: false}, data: action.users},
                next_link_users: action.link_next,
            });
        case MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_USERS_FAIL:
            return Object.assign({}, state, {
                users: {status:{req:false, ready: false, error: action.error}, data: {}},
                next_link_users: false,
            });
        case MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_MORE_USERS_REQUEST:
            return Object.assign({}, state, {
                more: null
            });
        case MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_MORE_USERS_OK:
            return Object.assign({}, state, {
                users: {status:{req: false, ready: true, error: false}, data: action.users},
                next_link_users: action.link_next,
                more: true
            });
        case MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_MORE_USERS_FAIL:
            return Object.assign({}, state, {
                users: {status:{req:false, ready: false, error: action.error}, data: {}},
                next_link_users: false,
                more: false

            });
        case MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_POSITIONS_REQUEST:
            return Object.assign({}, state, {
                positions: {status:{req:true, ready: null, error: null}, data:[]}
            });
        case MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_POSITIONS_OK:
            return Object.assign({}, state, {
                positions: {status:{req:false, ready: true, error: null}, data: action.positions}
            });
        case MAIN_PAGE_ACTIONS.MAIN_PAGE_GET_POSITIONS_FAIL:
            return Object.assign({}, state, {
                positions: {status:{req:false, ready: false, error: action.message}, data: []}
            });

        case MAIN_PAGE_ACTIONS.MAIN_PAGE_POST_NEW_USERS_REQUEST:
            return Object.assign({}, state, {
                new_user: {req:true, ready: null, error: null}
            });
        case MAIN_PAGE_ACTIONS.MAIN_PAGE_POST_NEW_USERS_OK:
            return Object.assign({}, state, {
                new_user: {req:false, ready: true, error: null}
            });
        case MAIN_PAGE_ACTIONS.MAIN_PAGE_POST_NEW_USERS_FAIL:
            return Object.assign({}, state, {
                new_user: {req:false, ready: false, error: action.error}
            });

        default:
            return state;
    }
};
export default mainPageReducer;
