import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {mapDispatchToProps, mapStateToProps} from './store/map_to_props';
import Banner from "./components/banner";
import AboutMe from "./components/about-me";
import MyRelationships from "./components/about-my-rel";
import Requirements from "./components/requirements";
import OurUsers from "./components/users";
import Registration from "./components/registration";
import PropTypes from 'prop-types'


class MainPage extends Component {

    state = {
        pageCount: 1,
        usersCount: 6,
        show_modal: false,

    };
    scrollY = 0;


    componentDidMount() {
        const {pageCount} = this.state;
        this.props.handleGetUsers();
        this.setState({pageCount: pageCount+1})
    }

    handleGetMoreUsers = () => {
        this.scrollY = window.scrollY;
        let {pageCount} = this.state;
        this.props.handleGetMoreUsers(pageCount, this.props.users.data);
        this.setState({pageCount: pageCount+1})
    };

    handleToggleModal = () => {
        this.setState({show_modal: !this.state.show_modal})
    };



    componentDidUpdate(prevProps, prevState, snapshot) {

        if (this.props.more && !prevProps.more)
        {
            window.scrollTo(0,this.scrollY);
        }
        if(this.props.new_user.ready && !prevProps.new_user.ready){
            this.setState({show_modal: !this.state.show_modal});
            this.props.handleGetUsers();
            this.setState({pageCount: 2})
        }
    }


    componentWillUnmount()
    {

    }

    render() {
        const {users, next_link_users, positions} = this.props;
        return (
            <Fragment>
                <Banner/>
                <AboutMe/>
                <MyRelationships/>
                <Requirements/>
                <OurUsers users={users} next_link_users={next_link_users} handleGetMoreUsers={this.handleGetMoreUsers}/>
                <Registration
                    handleGetPositions={this.props.handleGetPositions}
                    handlePostNewUser={this.props.handlePostNewUser}
                    positions={positions}
                    new_user={this.props.new_user}
                    show_modal = {this.state.show_modal}
                    handleToggleModal = {this.handleToggleModal}
                />
            </Fragment>
        );
    }
}

MainPage.propTypes = {
    users: PropTypes.object,
    next_link_users: PropTypes.bool,
    more: PropTypes.bool,
    handleGetUsers: PropTypes.func,
    handleGetMoreUsers: PropTypes.func,
    handleGetPositions: PropTypes.func,
    positions: PropTypes.object,
    handlePostNewUser: PropTypes.func,
    new_user: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);