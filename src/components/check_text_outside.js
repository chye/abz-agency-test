import React from 'react';

const checkElementTextOutSide = (text, maxLength, className="check-outside") => {
    const text_length = text.length;

    if(text_length > maxLength){
        let str = text.slice(0, maxLength);
        let a = str.split(' ');
        a.splice(a.length-1,1);
        str = a.join(' ');

        return <p className={'tooltip-hover'}>{`${str}...`}<span className="tooltip-block">{text}</span></p>

    }else{
        return <p className={className}>{text}</p>
    }
};
export default checkElementTextOutSide;