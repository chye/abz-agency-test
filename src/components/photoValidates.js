export const getFileSize = (size) => {
    let fSExt = ['Bytes', 'KB', 'MB', 'GB'];
    let i=0;
    while(size>900){size/=1024; i++;}
    const _size = (Math.round(size*100)/100);

    if(i < 2 || (i === 2 && _size <= 5) ){
        return {message:(Math.round(size*100)/100)+' '+fSExt[i], upTo: true};
    }else{
        return {message:'The photo may not be greater than 5 Mbytes', upTo: false}
    }

};

export const validateSize = (file) => {

    let reader = new FileReader();
    reader.readAsDataURL(file);
    let result ='';
    reader.onload = function (event) {
        let image = new Image();
        image.src = event.target.result;
        image.onload = function () {
            let height = this.height;
            let width = this.width;
            if (height >= 70 && width >= 70) {
                result =  {valid: true, message: ""}
            }else{
                result =  {valid: false, message: "the minimum size of 70x70px"}
            }
        };
        if(result !== ''){
            return result
        }
    };
};