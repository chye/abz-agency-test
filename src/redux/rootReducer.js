import {combineReducers} from 'redux';
import mainPageReducer from "../pages/main/store/reducer";
import loginLayoutReducer from  "../layout/login/store/reducer"

const  rootReducer = combineReducers({
    main: mainPageReducer,
    login: loginLayoutReducer
});

export  default rootReducer;